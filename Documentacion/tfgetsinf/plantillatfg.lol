\contentsline {lstlisting}{\numberline {3.1}Configuraci\IeC {\'o}n manual del m\IeC {\'o}dulo Neo6Mv2}{9}{lstlisting.3.1}
\contentsline {lstlisting}{\numberline {3.2}Cadenas NMEA sin Conexi\IeC {\'o}n}{9}{lstlisting.3.2}
\contentsline {lstlisting}{\numberline {3.3}Cadenas NMEA con Conexi\IeC {\'o}n}{9}{lstlisting.3.3}
\contentsline {lstlisting}{\numberline {3.4}Ejemplo de 'Hola mundo' en Laravel5}{10}{lstlisting.3.4}
\contentsline {lstlisting}{\numberline {3.5}Ejemplo de implementaci\IeC {\'o}n en Bootstrap}{11}{lstlisting.3.5}
\contentsline {lstlisting}{\numberline {3.6}Ejemplo de implementaci\IeC {\'o}n de una gr\IeC {\'a}fica con chart.js}{11}{lstlisting.3.6}
\contentsline {lstlisting}{\numberline {3.7}Ejemplo de implementaci\IeC {\'o}n de un mapa con la API de Google Maps}{12}{lstlisting.3.7}
\contentsline {lstlisting}{\numberline {4.1}C\IeC {\'o}digo de la Controladora Arduino Due}{20}{lstlisting.4.1}
\contentsline {lstlisting}{\numberline {4.2}Configuraci\IeC {\'o}n de la Controladora Arduino Due}{22}{lstlisting.4.2}
\contentsline {lstlisting}{\numberline {4.3}Conexi\IeC {\'o}n a la Red}{23}{lstlisting.4.3}
\contentsline {lstlisting}{\numberline {4.4}Funci\IeC {\'o}n \textit {LOOP} del c\IeC {\'o}digo}{23}{lstlisting.4.4}
\contentsline {lstlisting}{\numberline {4.5}Lectura en crudo del GPS}{24}{lstlisting.4.5}
\contentsline {lstlisting}{\numberline {4.6}Traducci\IeC {\'o}n y depuraci\IeC {\'o}n de los datos del GPS}{24}{lstlisting.4.6}
\contentsline {lstlisting}{\numberline {4.7}Formato de la String que se enviar\IeC {\'a}}{25}{lstlisting.4.7}
\contentsline {lstlisting}{\numberline {4.8}Funciones de recogida de datos}{25}{lstlisting.4.8}
\contentsline {lstlisting}{\numberline {4.9}Funci\IeC {\'o}n de comunicaci\IeC {\'o}n con el servidor}{26}{lstlisting.4.9}
\contentsline {lstlisting}{\numberline {4.10}Formato de la String GET}{26}{lstlisting.4.10}
\contentsline {lstlisting}{\numberline {4.11}Controlador que almacena la String en la BD}{26}{lstlisting.4.11}
\contentsline {lstlisting}{\numberline {4.12}Configuraci\IeC {\'o}n de la BD en Laravel}{27}{lstlisting.4.12}
\contentsline {lstlisting}{\numberline {4.13}Migraci\IeC {\'o}n en OSX}{28}{lstlisting.4.13}
\contentsline {lstlisting}{\numberline {4.14}Instalaci\IeC {\'o}n del Servicio Apache en Linux}{28}{lstlisting.4.14}
\contentsline {lstlisting}{\numberline {4.15}Asignaci\IeC {\'o}n de directorio}{28}{lstlisting.4.15}
\contentsline {lstlisting}{\numberline {4.16}Migraci\IeC {\'o}n en Debian}{28}{lstlisting.4.16}
\contentsline {lstlisting}{\numberline {5.1}Migraci\IeC {\'o}n de la base de datos}{30}{lstlisting.5.1}
\contentsline {lstlisting}{\numberline {5.2}Selector de Graficos}{34}{lstlisting.5.2}
