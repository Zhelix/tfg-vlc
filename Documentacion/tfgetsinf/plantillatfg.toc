\select@language {spanish}
\select@language {spanish}
\select@language {spanish}
\select@language {catalan}
\select@language {spanish}
\select@language {english}
\select@language {spanish}
\contentsline {chapter}{\'{I}ndice general}{\es@scroman {v}}{section*.1}
\contentsline {chapter}{\'{I}ndice de figuras}{\es@scroman {vii}}{section*.2}
\noindent \hrulefill \par 
\contentsline {chapter}{Listings}{\es@scroman {ix}}{section*.3}
\contentsline {chapter}{\numberline {1}Introducci\IeC {\'o}n}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Objetivos}{1}{section.1.1}
\contentsline {chapter}{\numberline {2}Estudio del estado del arte}{3}{chapter.2}
\contentsline {section}{\numberline {2.1}\IeC {\textquestiondown }Por qu\IeC {\'e} Arduino?}{3}{section.2.1}
\contentsline {section}{\numberline {2.2}Presupuesto}{4}{section.2.2}
\contentsline {section}{\numberline {2.3}Gesti\IeC {\'o}n del proyecto}{5}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}\textit {Bitbucket y SourceTree}}{5}{subsection.2.3.1}
\contentsline {chapter}{\numberline {3}Descripci\IeC {\'o}n del proyecto}{7}{chapter.3}
\contentsline {section}{\numberline {3.1}Tecnolog\IeC {\'\i }as utilizadas}{7}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Arduino}{7}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}GPS y NMEA Data}{8}{subsection.3.1.2}
\contentsline {subsubsection}{Formato del NMEA Data}{8}{subsubsection*.6}
\contentsline {subsection}{\numberline {3.1.3}Laravel}{9}{subsection.3.1.3}
\contentsline {subsection}{\numberline {3.1.4}MySQL}{10}{subsection.3.1.4}
\contentsline {subsection}{\numberline {3.1.5}Maps, Bootstrap y Chart.js}{10}{subsection.3.1.5}
\contentsline {chapter}{\numberline {4}Montaje del Hardware y del Servicio Web}{13}{chapter.4}
\contentsline {section}{\numberline {4.1}Material}{14}{section.4.1}
\contentsline {section}{\numberline {4.2}Montaje del Nodo Arduino DUE}{18}{section.4.2}
\contentsline {section}{\numberline {4.3}Programaci\IeC {\'o}n de la Microcontroladora Arduino DUE}{20}{section.4.3}
\contentsline {section}{\numberline {4.4}Instalaci\IeC {\'o}n del Servicio WEB}{27}{section.4.4}
\contentsline {subsection}{\numberline {4.4.1}Instalaci\IeC {\'o}n en MacOSX}{27}{subsection.4.4.1}
\contentsline {subsection}{\numberline {4.4.2}Instalaci\IeC {\'o}n en Linux}{28}{subsection.4.4.2}
\contentsline {chapter}{\numberline {5}Explicaci\IeC {\'o}n de la aplicaci\IeC {\'o}n}{29}{chapter.5}
\contentsline {section}{\numberline {5.1}Gesti\IeC {\'o}n de la informaci\IeC {\'o}n}{30}{section.5.1}
\contentsline {section}{\numberline {5.2}Gesti\IeC {\'o}n del \textit {NodeTracker}}{31}{section.5.2}
\contentsline {section}{\numberline {5.3}Visualizaci\IeC {\'o}n de la informaci\IeC {\'o}n}{33}{section.5.3}
\contentsline {subsection}{\numberline {5.3.1}Posicionamiento}{33}{subsection.5.3.1}
\contentsline {subsection}{\numberline {5.3.2}Generar un informe}{34}{subsection.5.3.2}
\contentsline {subsection}{\numberline {5.3.3}Seguimiento de la informaci\IeC {\'o}n}{34}{subsection.5.3.3}
\contentsline {chapter}{\numberline {6}Conclusiones y mejoras}{37}{chapter.6}
\contentsline {section}{\numberline {6.1}Seguridad}{37}{section.6.1}
\contentsline {section}{\numberline {6.2}Sensores}{37}{section.6.2}
\contentsline {section}{\numberline {6.3}Boards especializadas}{38}{section.6.3}
\contentsline {chapter}{Bibliograf\'{\i }a}{39}{chapter*.38}
