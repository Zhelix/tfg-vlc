

@extends('layouts.app')



@section('content')

    <body>


    <div class="container">

        <div>
            <canvas id="canvas" width="1300" height="400"></canvas>
        </div>
    </div>
    <script>



    var lineChartData = {
        labels: ["January", "February", "March", "April", "May", "June", "July"],
        datasets: [
            {
                data: [65, 59, 80, 81, 56, 55, 40],
            }
        ]
    };


    new Chart(document.getElementById('canvas').getContext('2d') , {
        type: "line",
        data: {
            labels: ["Ej1", "Ej2", "Ej3", "Ej4", "Ej5", "Ej6", "Ej7"],
            datasets: [
                {
                    data: [65, 59, 80, 81, 56, 55, 40],
                }
            ]
        }

    });

    </script>

@stop


